#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <cstdlib> 
#include <sstream>	//ostringstream
#include <iomanip>	//setprecision

#define max_l_cyfr 12
#define max_l_cyfr_pam 12
#define czyszczenie_ekranu system ("cls");

using namespace std;

class Kalkulator{
    void swap(char &pierwsza, char &druga);
    void pusty_kalk(char c[], int oraz_wypisz=1);
    string jaki_znak(char x);
    double konwert_do_liczba(char c[]);
    string konwert_do_string(double liczba);
    double dodawanie(double a, double b);
    double odejmowanie(double a, double b);
    double mnozenie(double a, double b);
    double dzielenie(double a, double b);
    double procent_dod(double a, double b);
    double procent_ode(double a, double b);
    double procent_mno(double a, double b);
    double procent_dziel(double a, double b);
    
    char c [max_l_cyfr];
	char znak;
    char ascii;
    double operand_1;
    double operand_2;
    string operat;
    double pamiec;
    
public:
    int pods_funk_kalk();
    Kalkulator();
};

Kalkulator::Kalkulator(){
	ascii='p';
    operand_1=0;
    operand_2=0;
    operat="pusty";
    pamiec = 0;
}

void Kalkulator::swap(char &pierwsza, char &druga) {
	char temp = druga;
	druga = pierwsza;
	pierwsza = temp;
}

											/*wyglad graficzny wyswietlacza przed wprowadzeniem cyfr*/
void Kalkulator::pusty_kalk(char c[], int oraz_wypisz){
	for (int i=0; i<(max_l_cyfr); ++i){
		c[i] = '_';
		if (oraz_wypisz) cout<<c[i];
	}
}

string Kalkulator::jaki_znak(char x){
	if (x > 47 && x<58) return "cyfra";
	else if (x==46) return "kropka";
	else if (x==42) return "mnozenie";
	else if (x==43) return "dodawanie";
	else if (x==45) return "odejmowanie";
	else if (x==47) return "dzielenie";
	else if (x==37) return "procent";
	else if (x==27) return "off";					//wyłączanie - znak "escape"
	else if (x==112) return "dod_do_pam";			//znak p na klawiaturze
	else if (x==117) return "usun_z_pam";			//znak u na klawiaturze
	else if (x==100) return "dod_do_dzial";			//znak d na klawiaturze
	else if (x==95) return "puste_miejsce";
	else if (x==99 || x==67) return "usuwanie";	//usuwanie - znak "c" lub "C"
	else if (x==13) return "rowna_sie";				//rowna_sie - znak "enter"
	else return "znak_zabroniony";
}

double Kalkulator::konwert_do_liczba(char c[]){
	int j=0;
	double liczba;
	char copy[max_l_cyfr];
	for (int i=0; i<(max_l_cyfr); ++i){
		if (c[i] != '_'){
			copy[j]=c[i];
			j++;
		}
	}
	
	liczba = atof (copy);
	return liczba;
}

string Kalkulator::konwert_do_string(double liczba){
	int j=(max_l_cyfr-1);
	int k=j;
	int ile_kopiowac=max_l_cyfr;
	int miejsce_kropki=-1;							//zakladamy ze kropki nie ma, czyli ze jest to liczba calkowita
	string copy;


//konwertujemy liczbe do string'a ustalajac precyzje liczb po przecinku na poziomie max_l_cyfr
    ostringstream ss;
	ss <<setprecision(max_l_cyfr)<< liczba;
	string s = ss.str();
	
	
//liczbe zapisana w string'u nalezy odpowiednio przyciac gdyz przed przecinkiem moga znajdowac sie cyfry 
//i liczba bedzie wystawala poza dopuszczalny bufor
//Trzeba usunac takze zera ktore moga zostac na koncu liczby zmiennoprzecinkowej po zrobieniu przyciecia
	for (int i=(max_l_cyfr-1); i>=0; --i){							//szukamy kropki
		if (s[i]=='.'){
			miejsce_kropki = i;						//jesli w liczbie jest kropka to zapisujemy indeks tablicy
			i=-1;									//znalezlismy kropke - wychodzimy z "for"
		}
	}
	
	if (miejsce_kropki != -1){						//tylko gdy kropka znajduje sie w liczbie
		while (s[j]=='0' && j>miejsce_kropki){		//szukamy zer za kropka na ostatnich pozycjach liczby zmiennoprzecinkowej
			j--;
		}
		
		ile_kopiowac = j+1;
		
		if (j==miejsce_kropki){						//za kropka sa same zera; bierzemy liczbe calkowita bez zer i kropki
			ile_kopiowac=miejsce_kropki;
		}										//w kazdym innym przypadku za kropka sa jakies cyfry rozne od zera; bierzemy liczbe zmiennoprzecinkowa ale bez zer na koncu
	}
	copy = s.substr(0,ile_kopiowac);		//kopiowanie cyfr do nowego string'a...
	
return copy;
}

double Kalkulator::dodawanie(double a, double b){
	double c = (a + b);
	return c;
}

double Kalkulator::odejmowanie(double a, double b){
	double c = (a - b);
	return c;
}

double Kalkulator::mnozenie(double a, double b){
	double c = (a * b);
	return c;
}

double Kalkulator::dzielenie(double a, double b){
	double  c = (a / b);
	return c;
}

double Kalkulator::procent_dod(double a, double b){
	double  c = (a + (b/100)*a);
	return c;
}

double Kalkulator::procent_ode(double a, double b){
	double  c = (a - (b/100)*a);
	return c;
}

double Kalkulator::procent_mno(double a, double b){
	double  c = a*(b/100);
	return c;
}

double Kalkulator::procent_dziel(double a, double b){
	double  c = a/(b/100);
	return c;
}

int Kalkulator::pods_funk_kalk(){
	
    
	cout<<"Kalkulator\nAby wykonac dzialania uzyj cyfr i znakow: \n/(dzielenie), \n*(mnozenie), \n-(odejmowanie), \n+(dodawanie), \nc(usun), \nEsc(off), \n.(separator dziesietny),\np - dodaj do pamieci,\nu - usun z pamieci,\nd-dodaj do dzialania\n";
	
		pusty_kalk(c,0);
		for(int i=0; i<(max_l_cyfr-1); ++i){	//wypisanie pustego miejsca (_) tyle razy ile pozostalo do wypelnienia calego bufora po wypisaniu liczby
		cout<<'_';
		}
		cout<<0;

											/*wprowadzanie cyfr z klawiatury*/
        	
	
	while (1) {
		for (int i = (max_l_cyfr-1); i>=0; --i) {
			if (ascii == 'p'){
				znak = _getch();
			} else{
				znak = ascii;
				ascii = 'p';
			}
			
			int it = i;
			if (jaki_znak(znak) == "cyfra" || jaki_znak(znak) == "kropka"){
				
				if (jaki_znak(znak) == "kropka" && i == (max_l_cyfr-1)){	//dodanie '0', gdy kropka wprowadzona na samym poczatku
					c[i]='.';
					i--;
					c[i]='0';
				}else{
					c[i]=znak;
					
					while (it != (max_l_cyfr-1)){
						swap(c[it], c[it+1]);
						it++;
					}
				}
				
				czyszczenie_ekranu
				
				for (int k=0; k<max_l_cyfr; ++k){
		        	(jaki_znak(c[k]) == "puste_miejsce" || jaki_znak(c[k]) == "kropka") ? cout<<c[k] : cout<<(c[k]-48);
				}
			}else if (jaki_znak(znak)== "znak_zabroniony"){
				i++;			//powtorzenie wprowadzania znaku
			} else if(jaki_znak(znak) == "usuwanie") {
				czyszczenie_ekranu
				pusty_kalk(c);
				operand_1=0;
				operand_2=0;
				operat="pusty";
				i=(max_l_cyfr);
			} else if (jaki_znak(znak) == "off") return 0;
			else if (jaki_znak(znak) == "dod_do_pam") {
				if (c[(max_l_cyfr-1)] != '_'){
					pamiec = konwert_do_liczba(c);
				}
			}else if (jaki_znak(znak) == "dod_do_dzial"){
				if (pamiec != 0){
					if (operand_1 != 0){
						operand_2 = pamiec;
					} else {
						operand_1 = pamiec;
					}
					
					czyszczenie_ekranu
					
					for(int i=0; i<(max_l_cyfr-(konwert_do_string(pamiec)).size()); ++i){	//wypisanie pustego miejsca (_) tyle razy ile pozostalo do wypelnienia calego bufora po wypisaniu liczby
						cout<<'_';
					}
					cout<<konwert_do_string(pamiec);		//po wypisaniu pustego miejsca(_), wypisanie liczby
				}
			}else if (jaki_znak(znak) == "usun_z_pam"){
				pamiec = 0;
			}else if (jaki_znak(znak) == "procent"){
				
				if (operand_1 != 0){
					operand_2 = konwert_do_liczba(c);
					
					if (operat=="dodawanie") operand_1 = procent_dod(operand_1, operand_2);
					else if (operat=="odejmowanie") operand_1 = procent_ode(operand_1, operand_2);
					else if (operat=="mnozenie")operand_1 = procent_mno(operand_1, operand_2);
					else if (operat=="dzielenie")operand_1 = procent_dziel(operand_1, operand_2);
					czyszczenie_ekranu
						for(int i=0; i<(max_l_cyfr-(konwert_do_string(operand_1)).size()); ++i){	//wypisanie pustego miejsca (_) tyle razy ile pozostalo do wypelnienia calego bufora po wypisaniu liczby
							cout<<'_';
						}
						cout<<konwert_do_string(operand_1);		//po wypisaniu pustego miejsca(_), wypisanie liczby
					
					pusty_kalk(c,0);
					operand_1 = 0;
					operand_2 = 0;
				}
			}
			
			else if(jaki_znak(znak) == "rowna_sie"){
				
				if (operand_1 != 0 && c[(max_l_cyfr-1)] != '_'){
						operand_2 = konwert_do_liczba(c);
				}
				
				if (operand_1 != 0 && operand_2 != 0){
					if (operat=="dodawanie") operand_1 = dodawanie(operand_1, operand_2);
					else if (operat=="odejmowanie") operand_1 = odejmowanie(operand_1, operand_2);
					else if (operat=="mnozenie")operand_1 = mnozenie(operand_1, operand_2);
					else if (operat=="dzielenie")operand_1 = dzielenie(operand_1, operand_2);
					
					czyszczenie_ekranu
					for(int i=0; i<(max_l_cyfr-(konwert_do_string(operand_1)).size()); ++i){	//wypisanie pustego miejsca (_) tyle razy ile pozostalo do wypelnienia calego bufora po wypisaniu liczby
						cout<<'_';
					}
					cout<<konwert_do_string(operand_1);		//po wypisaniu pustego miejsca(_), wypisanie liczby
					//operand_1=0;
					//operand_2=0;
					
				}
				pusty_kalk(c,0);
				i=(max_l_cyfr);	
			}
			else if (jaki_znak(znak) == "dodawanie" || jaki_znak(znak) == "odejmowanie" ||jaki_znak(znak) == "mnozenie" || jaki_znak(znak) == "dzielenie"){
				if (c[(max_l_cyfr-1)]){		//jesli w buforze jest jakas cyfra to...
				
					if (operand_1 != 0 && c[(max_l_cyfr-1)] != '_'){
						operand_2 = konwert_do_liczba(c);
					}
					
					if (operand_1 == 0 && c[(max_l_cyfr-1)] != '_'){	//jesli rowne '_' to znaczy ze dzialanie juz bylo wprowadzone a cyfra nie
						
						operand_1 = konwert_do_liczba(c);	
					}
				
					if (operand_2 == 0) {												//pierwsze wcisniecie znaku dzialania
						
						czyszczenie_ekranu						//wypisanie znaku dzialania, ale tylko gdy operand_2 jest nieznany
						for(int i=0; i<(max_l_cyfr-1); ++i){	//..
							cout<<'_';							//..
						}										//..
						cout<<znak;								//..
					
																						//drugie wcisniecie znaku dzialania
					} else if (operand_2 != 0 && c[(max_l_cyfr-1)] != '_' ){			//operand_2 wprowadzony i w buforze nie ma znaku '_' czyli ostatnio wprowadzona cyfra a nie znak dzialania
						if (operat=="dodawanie") operand_1 = dodawanie(operand_1, operand_2);
						else if (operat=="odejmowanie") operand_1 = odejmowanie(operand_1, operand_2);
						else if (operat=="mnozenie")operand_1 = mnozenie(operand_1, operand_2);
						else if (operat=="dzielenie")operand_1 = dzielenie(operand_1, operand_2);
						
						czyszczenie_ekranu
						for(int i=0; i<(max_l_cyfr-(konwert_do_string(operand_1)).size()); ++i){	//wypisanie pustego miejsca (_) tyle razy ile pozostalo do wypelnienia calego bufora po wypisaniu liczby
							cout<<'_';
						}
						cout<<konwert_do_string(operand_1);		//po wypisaniu pustego miejsca(_), wypisanie liczby
					} 
					operat = jaki_znak(znak);	//zapamietanie dzialania; dopiero po wykonaniu operacji, gdyz potrzebny stary operat
					pusty_kalk(c,0);
				}
				i=(max_l_cyfr);		//petla od nowa - wprowadzanie drugiej liczby
			}
	    }
	    ascii = _getch();
	    while (jaki_znak(ascii) == "znak_zabroniony"){
		ascii = _getch();
		}
	}
	return 0;
}
int main() {
	
	Kalkulator casio;
	casio.pods_funk_kalk();

  return 0;

}
